package com.soprasteria.component.person;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.soprasteria.model.json.person.Person;

public class PersonComponent implements Callable{
	public Object onCall(MuleEventContext eventContext) throws Exception {
		Person person = (Person) eventContext.getMessage().getPayload();
		return generateInformation(person);
	}
	
	private String generateInformation(Person person){
		return person.getName()+";"+person.getSurname()+";"+person.getAge();
	}
}
