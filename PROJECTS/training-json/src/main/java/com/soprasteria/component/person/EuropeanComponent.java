package com.soprasteria.component.person;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import com.soprasteria.model.json.person.European;

public class EuropeanComponent implements Callable {

	public Object onCall(MuleEventContext eventContext) throws Exception {
		European euroEntity = (European) eventContext.getMessage().getPayload();
		return generateInformation(euroEntity);
	}
	
	private String generateInformation(European euroEntity){
		return euroEntity.getCompetition()+";"+euroEntity.getNumber_teams();
	}
}
