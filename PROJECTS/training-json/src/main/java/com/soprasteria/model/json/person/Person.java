package com.soprasteria.model.json.person;

public class Person {
	private String name;
	private String surname;
	private String age;
	private Structure structure;
	private java.util.List<List> list = new java.util.ArrayList<List>();
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public Structure getStructure() {
		return structure;
	}
	public void setStructure(Structure structure) {
		this.structure = structure;
	}
	public java.util.List<List> getList() {
		return list;
	}
	public void setList(java.util.List<List> list) {
		this.list = list;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", surname=" + surname + ", age=" + age + ", structure=" + structure + ", list="
				+ list + "]";
	}
}

class Structure{
	private String value;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "Structure [value=" + value + "]";
	}
}

class List{
	private String record;
	private String value;
	
	public String getRecord() {
		return record;
	}
	public void setRecord(String record) {
		this.record = record;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "List [record=" + record + ", value=" + value + "]";
	}
}
