package com.soprasteria.model.json.person;

public class European {
	private String competition;
	private String number_teams;
	private java.util.List<Teams> teams = new java.util.ArrayList<Teams>();
	
	public String getCompetition() {
		return competition;
	}
	public void setCompetition(String competition) {
		this.competition = competition;
	}
	public String getNumber_teams() {
		return number_teams;
	}
	public void setNumber_teams(String number_teams) {
		this.number_teams = number_teams;
	}
	public java.util.List<Teams> getTeams() {
		return teams;
	}
	public void setTeams(java.util.List<Teams> teams) {
		this.teams = teams;
	}
	@Override
	public String toString() {
		return "EuropeanEntity [competition=" + competition + ", number_teams=" + number_teams + ", teams=" + teams
				+ "]";
	}
}

class Teams{
	private String name;
	private String national;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNational() {
		return national;
	}
	public void setNational(String national) {
		this.national = national;
	}
	@Override
	public String toString() {
		return "Teams [name=" + name + ", national=" + national + "]";
	}
}
