#Creazione tabella Person
CREATE TABLE person(
	id 				INT PRIMARY KEY AUTO_INCREMENT,
	name 			VARCHAR(50),
	surname 		VARCHAR(50),
	age 			TINYINT
);

#Inserimento dati
INSERT INTO person (name, surname, age) VALUES ('Gabriele', 'Granata', 22);
INSERT INTO person (name, surname, age) VALUES ('Davide', 'Zaza', 34);
INSERT INTO person (name, surname, age) VALUES ('Valentina', 'Rossetti', 19);
INSERT INTO person (name, surname, age) VALUES ('Andrea', 'Portaluppi', 21);

#Rimozione dati
DELETE FROM person where id = 2;ù

#Aggiornamento dati
UPDATE person set age = 18 where name='Valentina';