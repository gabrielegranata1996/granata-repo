jfilehelpers 

What is jfilehelpers ?

I file testuali strutturati vengono chiamati flat files (file piatti), per lavorarci, fino adesso, utilizzavamo uno script che li lavorasse.
Dovevi aprire il file, creare un lettore, quindi iniziare a leggere e analizzare riga per riga. 

JFileHelpers è una libreria che automatizza il noioso compito di analizzare e creare file di testo strutturati. 
Gestisce file a larghezza fissa o delimitati con annotazioni Java.
	
	
So what's code like?


Prendiamo, ad esempio, un file di testo strutturato a lunghezza fissa, che gestisce i dati dei clienti

@FixedLengthRecord()
public class Customer {

    @FieldFixedLength(4)
    public Integer custId;

    @FieldAlign(alignMode=AlignMode.Right)
    @FieldFixedLength(20)
    public String name;

    @FieldFixedLength(3)
    public Integer rating;

    @FieldTrim(trimMode=TrimMode.Right)
    @FieldFixedLength(10)
    @FieldConverter(converter = ConverterKind.Date,
        format = "dd-MM-yyyy")
    public Date addedDate;

    @FieldFixedLength(3)
    @FieldOptional
    public String stockSimbol;
}


Questo snippet di codice potrebbe gestire un file di testo strutturato in questo modo:

....|....1....|....2....|....3....|....4
1   Antonio Pereira     10012-12-1978ABC
2   Felipe Coury          201-01-2007
3   Anderson Polga       4212-11-2007DEF


